import { Template } from 'meteor/templating';

Template.addGame.helpers({
    settings() {
      return {
        position: "bottom",
        limit: 5,
        rules: [
          {
            token: '',
            collection: Meteor.users,
            field: "username",
            template: Template.userPill
          }
        ]
      };
  },
});

Template.addGame.events({

  'submit .new-match'(event, template) {
    console.log(this);
    // Prevent default browser form submit
    event.preventDefault();

    console.log(event);

    // Get value from form element
    // const target = event.target;
    // const text = target.text.value;
    const bestOf = template.find('#bestof').value;
    const gamesTo = template.find('#firstto').value;
    const player1 = template.find('#player1').value;
    const player2 = template.find('#player2').value;

    console.log('games.insert', bestOf, gamesTo, player1, player2);
    // Insert a task into the collection
    Meteor.call('match.insert', bestOf, gamesTo, player1, player2, function(error, result) {

      if(result) {
        Meteor.call('games.insert', player1, player2, '', '', [], result, bestOf, gamesTo, function (err, res) {
          if(err) {

          } else {
            Router.go('/matches/' + result);
          }
        });
      }

    });

  }

});
