import { Template } from 'meteor/templating';

import { Games } from '../api/games.js';
import { Players } from '../api/players.js';
import { Matches } from '../api/matches.js';

import './body.html';
import './add-game.html';
import './player.html';
import './match.html';
import './live-game.html';

import './add-game.js';
import './match.js';
import './live-game.js';
import './player.js';

Template.Home.helpers({

  games() {
    //console.log(Games.find());
    return Games.find({});
  },

  matches() {
    //console.log(Matches.find({}))
    return Matches.find({}, {sort: {"started": -1}});
  },

  players() {
    //console.log(Meteor.users.find({}));
    // this type of sort is necessary
    return Meteor.users.find({}, {sort: {"profile.rating": -1}});
  }
});

Template.player.helpers({
  // gameWins() {
  //   console.log(this.profile.gameWins);
  // }
  pointsFor(user) {
    //console.log(this);
    if(user) {
      let winningGames = Games.find({$or: [{player1: user}, {player2: user}]});
      let i = 0;
      let total = 0;

      winningGames.forEach(function (row) {
        if(row.player1 != user) {
          i = 1;
        } else {
          i = 0;
        }
        total += row.scores[i];
      });
      return total;
    }
  },

  pointsAgainst(user) {
    //console.log(this);
    if(user) {
      let winningGames = Games.find({$or: [{player1: user}, {player2: user}]});
      let i = 0;
      let total = 0;

      winningGames.forEach(function (row) {
        if(row.player1 != user) {
          i = 0;
        } else {
          i = 1;
        }
        total += row.scores[i];
      });
      return total;
    }
  }
});
