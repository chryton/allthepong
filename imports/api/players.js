import { Mongo } from 'meteor/mongo';

Meteor.methods({
  'player.addGameWin' (user) {
    Meteor.users.update({username: user}, {
      $inc: {
        "profile.gameWins": 1,
        "profile.totalGames": 1
      }
    });
  },

  'player.addMatchWin' (user, ratingInc) {
    Meteor.users.update({username: user}, {
      $inc: {
        "profile.matchWins": 1,
        "profile.totalMatches": 1,
        "profile.rating": ratingInc
      }
    });
  },

  'player.addGameLoss' (user) {
    Meteor.users.update({username: user}, {
      $inc: {
        "profile.gameLosses": 1,
        "profile.totalGames": 1
      }
    });
  },

  'player.addMatchLoss' (user, ratingInc) {
    Meteor.users.update({username: user}, {
      $inc: {
        "profile.matchLosses": 1,
        "profile.totalMatches": 1,
        "profile.rating": ratingInc
      }
    });
  }
});
