import { Mongo } from 'meteor/mongo';

export const Matches = new Mongo.Collection('matches');

Meteor.methods({
  'match.insert'(bestOf, gamesTo, player1, player2, callback) {
    let result = Matches.insert({
      started: new Date(),
      submitter: Meteor.users.findOne(this.userId).username,
      player1: player1,
      player2: player2,
      bestOf: bestOf,
      gamesTo: gamesTo,
      scores: [],
      winner: '',
      loser: ''
    });

    return result;

    // for(let i = 0; i < bestOf; i++) {
    //   Games.insert({

    //   })
    // }
  },
  'match.update' (matchID, scores, matchWinner, matchLoser, rating1, rating2) {
    Matches.update(matchID, {
      $set: {
        scores: scores,
        winner: matchWinner,
        loser: matchLoser,
        rating1: rating1,
        rating2: rating2
      }
    });
  }
});
