import { Mongo } from 'meteor/mongo';

export const Games = new Mongo.Collection('games');

Meteor.methods({
  'games.insert' (player1, player2, winner, loser, scores, matchID, bestOf, gamesTo) {
    for(let i = 0; i < bestOf; i++) {
      Games.insert({
        played: new Date(),
        owner: this.userId,
        submitter: Meteor.users.findOne(this.userId).username,
        player1: player1,
        player2: player2,
        winner: winner,
        loser: loser,
        scores: scores,
        matchID: matchID,
      });
    }
  },

  'games.update' (gameID, scores, winner, loser) {
    Games.update(gameID, {
      $set: {
        scores: scores,
        winner: winner,
        loser: loser
      }
    });
  }

});
