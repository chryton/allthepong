import { Matches } from '/imports/api/matches.js';
import { Games } from '/imports/api/games.js';

Router.route('/', function () {
  this.render('Home');
});

Router.route('/canary', function () {
  this.render('Home');
});


Router.route('/add-game', function () {
  this.render('add-game');
});

Router.route('/player/:id', {
  template: 'unique-player',
  data: function(){
    var currentList = this.params.id;
    return Meteor.users.findOne({ _id: currentList });
  }
});

Router.route('/matches/:id', {
  template: 'unique-match',
  data: function(){
    var currentList = this.params.id;
    return Matches.findOne({ _id: currentList });
  }
});

Router.route('/live/:id', {
  template: 'live-game',
  data: function(){
    var currentList = this.params.id;
    return Games.findOne({ _id: currentList });
  }
});
