import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
});

import '../imports/api/games.js';
import '../imports/api/matches.js';
import '../imports/api/players.js';


import { Accounts } from 'meteor/accounts-base';

Accounts.onCreateUser(function(options, user) {

  user.profile = {
    gameWins: 0,
    gameLosses: 0,
    matchWins: 0,
    matchLosses: 0,
    rating: 1000 // this will then have a fluctuation for ratings
  };

  // We still want the default hook's 'profile' behavior.
  // if (options.profile)
  //   user.profile = options.profile;
  return user;
});
